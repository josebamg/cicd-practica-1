# Integración y Despliegue Continuos - Práctica 1 Joseba Martin

![Made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-success)
![Gitlab code coverage](https://gitlab.com/josebamg/cicd-practica-1/badges/v1.0.0/pipeline.svg)
![Gitlab code coverage](https://gitlab.com/josebamg/cicd-practica-1/badges/v1.0.0/coverage.svg)

Cobertura para microservicio *Movie*:
![Gitlab code coverage](https://gitlab.com/josebamg/cicd-practica-1/badges/v1.0.0/coverage.svg?job=test%20movie)

Cobertura para microservicio *Cast*:
![Gitlab code coverage](https://gitlab.com/josebamg/cicd-practica-1/badges/v1.0.0/coverage.svg?job=test%20cast)

Primera práctica para la asignatura de Integración y Despliegue Continuo, utilizando como base una aplicación de basada
en microservicios para guardar y acceder a películas y al reparto de estas
([link](https://dev.to/paurakhsharma/microservice-in-python-using-fastapi-24cc)).

La información técnica del desarrollo viene detallada en el documento [doc.md](./doc.md).

## Changelog

- Refactorizados los microservicios a **Arquitectura Hexagonal**
- Desarrollado un **Conjunto de Pruebas Unitarias** para una cobertura mínima del 20%
- Desarrollado un **Conjunto de Pruebas de Humo** para una revisión rápida del producto
- **Nueva funcionalidad** añadida para recabar información de películas de una API siguiendo un **Desarrollo Guiado por
  Pruebas**
- Creado un **Pipeline de Integración Continua** de automatización de **Build** y **Test**, resultando en unos
  contenedores desplegables
- Realizado el **despliegue** automático en el entorno **Stage**
- Empleada una **Metodología Ágil** para la gestión de tareas en la práctica

## Autores

Esta práctica ha sido realizada por [Joseba Martin](https://gitlab.com/josebamg).

## Reconocimiento

Para la práctica, se ha utilizado como base
el [repositorio](https://github.com/paurakhsharma/python-microservice-fastapi) creado
por [Paurakh Sharma](https://github.com/paurakhsharma).