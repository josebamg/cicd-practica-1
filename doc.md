# Documentación técnica

![Made-with-Markdown](https://img.shields.io/badge/Made%20with-Markdown-success)

## Tabla de Contenido

- [Introducción](#introducción)
- [Conda: Creación y actualización de entornos](#conda-creación-y-actualización-de-entornos)
- [Metodología del Desarrollo](#metodología-de-desarrollo)
    - [Estrategia de Ramas](#estrategia-de-ramas)
- [Funcionalidad Inicial](#funcionalidad-inicial)
    - [Endpoints](#endpoints)
- [Refactorización del código a Arquitectura Hexagonal](#refactorización-del-código-a-arquitectura-hexagonal)
- [Conjunto de Pruebas Unitarias](#conjunto-de-pruebas-unitarias)
- [Desarrollo de una nueva funcionalidad mediante un Desarrollo Guiado por Pruebas](#desarrollo-de-una-nueva-funcionalidad-mediante-un-desarrollo-guiado-por-pruebas)
    - [Creación de pruebas](#creación-de-pruebas)
- [Pipeline de Integración Continua](#pipeline-de-integración-continua)
- [Conjunto de Pruebas de Humo](#conjunto-de-pruebas-de-humo)
- [Despliegue en el entorno Stage](#despliegue-en-el-entorno-stage)

## Introducción

Como objetivo de esta práctica se establece:

- Ser capaz de diseñar e implementar un **Pipeline de Integración Continua**

Así, y como pre-requisito, cada estudiante debe seleccionar un software basado en microservicios para la consecución de
la práctica. En este caso, el software utilizado como base se detalla en el [README](./README.md).

## Conda: Creación y actualización de entornos

Como gestor de entornos, se ha utilizado [Conda](https://docs.conda.io) en lugar de [Poetry](https://python-poetry.org).
Conda sirve tanto para la gestión de paquetes como de entornos, al contrario que Poetry, que solo ofrece la capacidad de
gestionar entornos de [Python](https://www.python.org), siendo así el primero más completo.

Así, para trabajar con Conda en este laboratorio, a continuación se detallan los diferentes comandos utilizados:

- Para crear diferentes entornos para producción o desarrollo:

```commandline
conda env create -f <environment-name>.yml
```

- Para utilizar el entorno creado (activarlo en el IDE también):

```commandline
conda activate <environment-name>
```

- Para actualizar un entorno:

```commandline
conda env update --prefix ./env --file <environment-name>.yml --prune
```

- Para exportar las dependencias del entorno a un archivo *requirements.txt* (teniendo [pip](https://pypi.org/)
  instalado en el entorno):

```commandline
pip list --format=freeze > requirements.txt
```

## Metodología de Desarrollo

En cuanto a la metodología del desarrollo, se ha planteado una Metodología Ágil, mediante el uso de las herramientas
ofrecidas por [GitLab](https://gitlab.com/).

El desarrollo se ha llevado a cabo en un sprint (*Milestone* en GitLab) o iteración, planteando primero las tareas
(*Issues* en GitLab) a desarrollar, y asignándolas al sprint después. Para el seguimiento de la consecución de las
tareas, se ha creado un tablero (*Board* en GitLab) asignado al sprint, pudiendo cambiar las tareas entre tres
diferentes estados: *Open* (no iniciada), *Doing* (en progreso) y *Closed* (finalizada). Las siguientes imágenes
muestran las tareas, el sprint y el tablero.

![Tareas](img/tasks.png "Tareas")

![Sprint](img/sprint.png "Sprint")

![Tablero](img/board.png "Tablero")

### Estrategia de Ramas

Respecto al sistema de control de versiones, se ha planteado una estrategia de ramas para el desarrollo con Git.

- Se cuenta con dos ramas principales, **main** y **dev**. En **main** se hará *merge* de los cambios y nuevas
  funcionalidades tras ser desarrolladas y probadas en **dev**.
- Todas las nuevas funcionalidades se desarrollan en diferentes ramas con el formato de nombre **
  feature/{feature_name}**, partiendo siempre desde la rama **dev**. Lo mismo ocurre con los arreglos de fallos, pero
  con el formato de nombre **bugfix/{bugfix_name}**.
- Respecto a las etiquetas (*tags*), estas seguirán un formato de tres números divididos por dos puntos (***X.X.X***).
  El primer número se incrementará tras la finalización de un sprint y al hacer *merge* a la rama **main**, el segundo
  tras la finalización del desarrollo de una funcionalidad en una rama **feature** y al hacer *merge* a **dev**, y el
  último al realizar una corrección de fallos en una rama **bugfix** y al hacer *merge* a **dev**.

La imagen a continuación muestra lo explicado de manera visual.

![Estrategia de Ramas](img/Git%20branching.png "Estrategia de Ramas")

## Funcionalidad inicial

El software inicial cuenta con dos principales microservicios: *Movie* y *Cast*. El microservicio de *Movie* guarda y
accede a películas en una base de datos, y el microservicio de *Cast* hace lo mismo con el reparto de las películas.

Todo el sistema se pone en marcha por medio del archivo [docker-compose.yml](./docker-compose.yml) local, mediante la
ejecución de contenedores en Docker para los microservicios, bases de datos y un servidor Nginx como proxy.

### Endpoints

Los **endpoint** ofrecidos por los microservicios son los siguientes:

- **Movie** (documentación en **endpoint** */api/v1/movie/docs*)

| **Nombre**   | **API Endpoint**      | **Método HTTP** | **Propósito**          |
|--------------|-----------------------|-----------------|------------------------|
| Get Movies   | */api/v1/movie/*      | `GET`           | Buscar películas       |
| Get Movie    | */api/v1/movie/{id}/* | `GET`           | Buscar película por ID |
| Create Movie | */api/v1/movie/*      | `POST`          | Crear película         |
| Update Movie | */api/v1/movie/{id}/* | `PUT`           | Actualizar película    |
| Delete Movie | */api/v1/movie/{id}/* | `DELETE`        | Borrar película por ID |

- **Cast** (documentación **endpoint** */api/v1/cast/docs*)

| **Nombre** | **API Endpoint**     | **Método HTTP** | **Propósito**  |
|------------|----------------------|-----------------|----------------|
| Get Cast   | */api/v1/cast/{id}/* | `GET`           | Buscar reparto |
| Post Cast  | */api/v1/cast/*      | `POST`          | Añadir reparto |

## Refactorización del código a Arquitectura Hexagonal

Como punto previo al desarrollo, el código ha sido refactorizado a una Arquitectura Hexagonal, un tipo de arquitectura
limpia que busca aislar la lógica de negocio de una aplicación de dependencias externas (bases de datos, interfaces de
usuario, servicios de terceros...). Esto hace que el código sea más testable, flexible, mantenible, escalable y
reusable.

Además, es ideal para el desarrollo de la nueva funcionalidad planteada, que tiene como objetivo poder recabar
información de servicios de terceros. Así, mediante la implementación de interfaces en la lógica de negocio, se puede
realizar un desarrollo desacoplado de dependencias externas, siendo el desarrollo de tests más sencillo.

La siguiente imagen muestra un esquema de una arquitectura limpia (**Clean Architecture**).

![Clean Architecture](img/CleanArchitecture.jpg "Arquitectura Limpia")

Una explicación más detallada de la arquitectura puede ser encontrada
en [The Clean Code Blog](https://blog.cleancoder.com/uncle-bob/2012/08/13/the-clean-architecture.html).

En este caso concreto, se ha planteado una estructura de carpetas de la siguiente forma:

![Estructura de Carpetas](img/FolderStructure.png "Estructura de Carpetas")

En la que la capa externa implementando servicios de terceros se encuentra en la carpeta **infrastructure**, la capa de
adaptadores de interfaces se encuentra en la carpeta de **application**, y por último las dos capas correspondientes a
la lógica de negocio (entidades, interfaces propias, casos de uso...) se encuentran en la carpeta **domain**. Así, en la
carpeta **infrastructure** se encuentran las implementaciones de las interfaces propias, haciendo uso de librerías y
controladores de terceros.

## Conjunto de Pruebas Unitarias

Para probar las funcionalidades detalladas en el apartado [*Funcionalidad Inicial*](#funcionalidad-inicial), se ha
desarrollado un Conjunto de Pruebas Unitarias para cada microservicio (*Movie* y *Cast*). Para ello, primero se han
desarrollado pruebas para los **endpoints**, utilizando *monkeypatch* para simular los servicios de los que dependen, y
después pruebas para dichos servicios, que guardan, acceden y editan la base de datos.

Para las pruebas de los **endpoints**, en el microservicio de *Cast* se ha simulado el servicio *PSQLCastService*:

```python
class MockPSQLCastService:
    def __init__(self, *args, **kwargs):
        self.cast_out = CastOut(
            id=cast_id, name=name, summary=summary, created_at=created_at
        )

    async def get_by_id(self, *args, **kwargs):
        return self.cast_out

    async def add(self, *args, **kwargs):
        return self.cast_out
```

Y en el de *Movie* el servicio *PSQLMovieService*:

```python
class MockPSQLMovieService:
    def __init__(self, *args, **kwargs):
        self.movie_out = MovieOut(
            id=1,
            name=name,
            plot=plot,
            genres=genres,
            cast_ids=cast_ids,
            created_at=created_at,
        )

    async def get_by_id(self, *args, **kwargs):
        return self.movie_out

    async def add(self, *args, **kwargs):
        return self.movie_out

    async def get_all(self, *args, **kwargs):
        return [self.movie_out]

    async def delete(self, *args, **kwargs):
        return self.movie_out

    async def update(self, *args, **kwargs):
        return self.movie_out
```

Tras esto, las pruebas desarrolladas prueban todos los *endpoints* de cada microservicio, probando que devuelven el
código de estado y la respuesta correctos. En el de *Cast*:

```python
@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_create_cast(test_client, monkeypatch, anyio_backend):
    monkeypatch.setattr(
        "app.application.api.cast_router.PSQLCastService", MockPSQLCastService
    )

    response = test_client.get(f"/api/v1/cast/{cast_id}/")
    assert response.status_code == 200
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_get_cast(test_client, monkeypatch, anyio_backend):
    cast_in = {"name": name, "summary": summary}

    monkeypatch.setattr(
        "app.application.api.cast_router.PSQLCastService", MockPSQLCastService
    )

    response = test_client.post("/api/v1/cast/", json=cast_in)
    assert response.status_code == 201
    assert response.json() == test_data
```

En el de *Movie*:

```python
@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_get_movies(test_client, monkeypatch, anyio_backend):
    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.get("/api/v1/movie/")
    assert response.status_code == 200
    assert response.json() == [test_data]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_get_movie(test_client, monkeypatch, anyio_backend):
    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.get(f"/api/v1/movie/{movie_id}/")
    assert response.status_code == 200
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_add_movie(test_client, monkeypatch, anyio_backend):
    movie_in = {"name": name, "plot": plot, "genres": genres, "cast_ids": cast_ids}

    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.post("/api/v1/movie/", json=movie_in)
    assert response.status_code == 201
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_update_movie(test_client, monkeypatch, anyio_backend):
    movie_update = {"name": name, "plot": plot, "genres": genres, "cast_ids": cast_ids}

    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.put(f"/api/v1/movie/{movie_id}/", json=movie_update)
    assert response.status_code == 200
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_delete_movie(test_client, monkeypatch, anyio_backend):
    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.delete(f"/api/v1/movie/{movie_id}/")
    assert response.status_code == 200
    assert response.json() == test_data
```

Respecto a las pruebas de los servicios que acceden, editan y guardan en la base de datos, se han desarrollado pruebas
que comprueban el funcionamiento de los servicios anteriormente simulados mediante *monkeypatch*. En el microservicio
de *Cast*:

```python
@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_create_and_get_cast(test_db, anyio_backend):
    cast_service = PSQLCastService(PSQLCastRepository(test_db))

    cast_in = CastIn(name=name, summary=summary)

    cast_out = await cast_service.add(cast_in=cast_in)

    assert cast_out is not None
    assert cast_out.name == name

    cast_out = await cast_service.get_by_id(id=1)
    assert cast_out is not None
    assert cast_out.name == name
    assert cast_out.id == 1


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_cast_not_found_exception_thrown(test_db, anyio_backend):
    cast_service = PSQLCastService(PSQLCastRepository(test_db))

    with pytest.raises(CastNotFoundException):
        await cast_service.get_by_id(id=1)

```

En el microservicio de *Movie*:

```python
@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_create_and_get_movie(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    movie_in = MovieIn(name=name, genres=genres, plot=plot, cast_ids=cast_ids)

    movie_out = await movie_service.add(movie_in=movie_in)

    assert movie_out is not None
    assert movie_out.name == name

    movie_out = await movie_service.get_by_id(id=1)

    assert movie_out is not None
    assert movie_out.name == name
    assert movie_out.id == 1


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_get_movies(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    movies_out = await movie_service.get_all()

    assert movies_out is not None
    assert len(movies_out) == 0

    movie_in = MovieIn(name=name, genres=genres, plot=plot, cast_ids=cast_ids)
    movie_out = await movie_service.add(movie_in=movie_in)

    movies_out = await movie_service.get_all()

    assert movies_out is not None
    assert movies_out[0].name == movie_out.name


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_update_movie(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    movie_in = MovieIn(name=name, genres=genres, plot=plot, cast_ids=cast_ids)
    movie_update = MovieIn(
        name="Kill Bill", genres=genres, plot=plot, cast_ids=cast_ids
    )

    movie_out = await movie_service.add(movie_in=movie_in)
    movie_updated = await movie_service.update(
        id=movie_out.id, movie_update=movie_update
    )

    assert movie_updated is not None
    assert movie_updated.id == movie_out.id
    assert movie_updated.name != movie_out.name


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_delete_movie(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    movie_in = MovieIn(name=name, genres=genres, plot=plot, cast_ids=cast_ids)

    movie_out = await movie_service.add(movie_in=movie_in)
    movies_out = await movie_service.get_all()

    assert movies_out is not None
    assert len(movies_out) == 1

    movie_deleted = await movie_service.delete(id=movie_out.id)
    movies_out = await movie_service.get_all()

    assert movie_deleted is not None
    assert movie_deleted.id == 1
    assert len(movies_out) == 0


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_movie_not_found_exception_thrown(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    with pytest.raises(MovieNotFoundException):
        await movie_service.get_by_id(id=1)
```

Con este Conjunto de Pruebas Unitarias, se ha logrado un porcentaje de cobertura del código del 83% en el microservicio
de *Cast*:

![Cobertura de código inicial para *Cast*](img/Cobertura%20inicial%20Cast.png "Cobertura de código inicial para *Cast*")

En el de *Movie*, se ha conseguido una cobertura del 82%:

![Cobertura de código inicial para *
Movie*](img/Cobertura%20inicial%20Movie.png "Cobertura de código inicial para *Movie*")

Respecto a la Metodología Ágil, así queda el sprint:

![Sprint tras Test Suite](img/sprint%20end%20test%20suite.png "Sprint tras Test Suite")

Y el tablero:

![Tablero tras Test Suite](img/board%20end%20test%20suite.png "Tablero tras Test Suite")

## Desarrollo de una nueva funcionalidad mediante un Desarrollo Guiado por Pruebas

La funcionalidad a desarrollar consiste en un nuevo *endpoint* que permite buscar películas por título en la base de
datos. En caso de no haber ninguna que cumpla con los requisitos de la búsqueda en la base de datos, se realizará una
búsqueda en una API de terceros, que devolverá las películas tras ser guardadas (además de guardar el reparto de cada
una en el microservicio *Cast* también). También se han desarrollado *health-checks* para cada microservicio. Los
*endpoint* añadidos en el microservicio de *Movie*:

| **Nombre**          | **API Endpoint**                      | **Método HTTP** | **Propósito**               |
|---------------------|---------------------------------------|-----------------|-----------------------------|
| Get Movies by Title | */api/v1/movie/search/?title={title}* | `GET`           | Buscar películas por título |
| Health Check        | */api/v1/movie/health-check/*         | `GET`           | Prueba de salud             |

En el de *Cast*:

| **Nombre**          | **API Endpoint**             | **Método HTTP** | **Propósito**               |
|---------------------|------------------------------|-----------------|-----------------------------|
| Health Check        | */api/v1/cast/health-check/* | `GET`           | Prueba de salud             |

### Creación de pruebas

Con esta metodología, antes de iniciar el desarrollo se implementan una serie de pruebas de integración, que primero
fallarán al no estar desarrolladas las funcionalidades. Así, el objetivo es llegar al punto de que no fallen.

Primero se han implementado pruebas para los diferentes *endpoints* a desarrollar. Las pruebas para los *endpoints* de
*health-check* son ambas iguales, cambiando en cada caso el **URL** de la base de datos en cada microservicio:

```python
@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_health_check(test_client, anyio_backend):
    response = test_client.get("/api/v1/cast/health-check/")
    assert response.status_code == 200
    assert response.json() == {
        "message": "OK",
        "environment": "local",
        "debug": True,
        "test": True,
        "database_url": "postgresql+asyncpg://cast_db_username:cast_db_password@cast_db/cast_db_dev",
    }
```

Respecto al *endpoint* para recabar información de películas por título, esta es la prueba implementada:

```python
@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_get_movies_by_title(test_client, monkeypatch, anyio_backend):
    name = "Inception (2010)"

    response = test_client.get(f"/api/v1/movie/search?title={name}")

    assert response.status_code == 201
    assert len(response.json()) > 0

    response = test_client.get(f"/api/v1/movie/search?title={name}")

    assert response.status_code == 200
    assert response.json()[0]["name"] == name
```

Esencialmente, lo que la prueba trata de hacer es realizar una búsqueda por título de películas, y como al iniciar las
pruebas la base de datos está vacía, estas se guardarán en los microservicios. Así, la primera vez que se ejecuta la
búsqueda por título, el *endpoint* debe devolver un código de respuesta de 201 (**CREATED**), indicando que se han
creado las películas obtenidas por la API de terceros. La segunda vez que se ejecuta, el código debería de cambiar a
200, devolviendo la lista de películas creadas antes. En este caso, solo debería de crearse una película: "Inception (
2010)".

Con esto, al ejecutar las pruebas, se obtiene el siguiente resultado:

![Fallo de pruebas en *Cast*](img/TDD%20Test%20Fail%20Cast.png "Fallo de pruebas en *Cast*")

![Fallo de pruebas en *Movie*](img/TDD%20Test%20Fail%20Movie.png "Fallo de pruebas en *Movie*")

Es decir, las pruebas fallan. Primero se ha desarrollado la funcionalidad para los *health-check*. Para ello, se ha
creado un nuevo *router*, y añadido este a la aplicación de **FastAPI** en ambos microservicios:

```python
@health_router.get("/health-check/")
def healthcheck(config: BaseConfig = Depends(load_config)):
    return {
        "message": "OK",
        "environment": config.APP_ENVIRONMENT,
        "debug": config.DEBUG,
        "test": config.TESTING,
        "database_url": config.SQLALCHEMY_DATABASE_URL,
    }
```

Y en la función *create_app* de ambos microservicios:

```python
server.include_router(health_router, prefix="/api/v1/movie", tags=["health check"])
```

Habiendo implementado los *health-check*, solo queda implementar el *endpoint* de búsqueda, siendo su prueba de
integración la que resta por ser implementada:

![Health-check en *Cast*](img/TDD%20Test%20Health%20Check%20Cast.png "Health-check en *Cast*")

![Health-check en *Movie*](img/TDD%20Test%20Health%20Check%20Movie.png "Health-check en *Movie*")

Para ello, primero se crea una interfaz para el servicio, que luego habrá que implementar utilizando una API (en este
caso la de [IMDB](https://imdb-api.com/)):

```python
class MovieAPIService(abc.ABC):
    @abc.abstractmethod
    async def search_by_title(self, name: str) -> List[MovieOut]:
        raise NotImplementedError 
```

Métodos similares se han añadido también a las interfaces del repositorio y servicio para buscar películas en el
microservicio:

```python
class MovieRepository(abc.ABC):
    ...

    @abc.abstractmethod
    async def search_by_title(self, name: str) -> List[Movie]:
        raise NotImplementedError 
```

```python
class MovieService(abc.ABC):
    ...

    @abc.abstractmethod
    async def search_by_title(self, name: str) -> List[MovieOut]:
        raise NotImplementedError 
```

La implementación de estos métodos son las siguientes:

```python
class PSQLMovieRepository(MovieRepository, ABC):
    ...

    async def search_by_title(self, name: str):
        stmt = select(Movie).where(Movie.name.contains(name))
        movies = await self.session.execute(stmt)
        return movies.scalars().all()
```

```python
class PSQLMovieService(MovieService, ABC):
    ...

    async def search_by_title(self, name: str):
        movies = await self.repository.search_by_title(name=name)
        movies_out = list(map(movie_to_out, movies))
        return movies_out
```

Y la implementación del servicio que utiliza la API de IMDB:

```python
class IMDBMovieAPIService(MovieAPIService, ABC):
    def __init__(self, repository: MovieRepository):
        self.repository = repository

    async def search_by_title(self, name: str) -> List[MovieOut]:
        response = httpx.get(
            f"https://imdb-api.com/en/API/SearchMovie/{IMDB_API_KEY}/{name}"
        )

        response_json = response.json()

        if response_json["errorMessage"] != "":
            raise MovieNotFoundException(
                "Movies with the given title could not be found in the DB, "
                f"and the IMDB API gave the following error: {response_json['errorMessage']}"
            )

        movies = []

        for result in response.json()["results"]:
            movie_response = httpx.get(
                f"https://imdb-api.com/en/API/Title/{IMDB_API_KEY}/{result['id']}"
            )
            movie_json = movie_response.json()
            cast_ids = []

            if movie_json["starList"] is not None:
                for star in movie_json["starList"]:
                    star_response = httpx.get(
                        f"https://imdb-api.com/en/API/Name/{IMDB_API_KEY}/{star['id']}"
                    )

                    star_json = star_response.json()

                    cast = add_cast(
                        cast_in={
                            "name": star_json["name"],
                            "summary": star_json["summary"],
                        }
                    )

                    cast_ids.append(cast["id"])

            genres = []

            if movie_json["genres"] is not None:
                genres = movie_json["genres"].split(", ")

            movie = await self.repository.add(
                name=movie_json["fullTitle"],
                plot=movie_json["plot"],
                genres=genres,
                cast_ids=cast_ids,
            )

            if isinstance(movie, Movie):
                movies.append(movie)

        movies_out = list(map(movie_to_out, movies))

        return movies_out
```

En caso de error, la API devuelve un mensaje de error, por lo que para esos casos se lanza una excepción propia:

```python
class MovieAPIException(Exception):
    pass
```

Respecto a las demás llamadas a la API, se ha utilizado su [documentación](https://imdb-api.com/api/) para saber qué
*endpoints* utilizar para recabar la información necesaria para los microservicios.

Para añadir nuevos repartos que no existan en el microservicio *Cast*, se ha creado una función que hace la llamada al
*endpoint* para añadir reparto:

```python
def add_cast(cast_in: dict):
    r = httpx.post(f"{CAST_SERVICE_HOST_URL}", json=cast_in)
    return r.json()
```

Además, se ha cambiado la funcionalidad de añadir reparto en el microservicio *Cast*, para que en caso de que
exista una entrada con el mismo nombre no se duplique y devuelva la entidad existente:

```python
class PSQLCastRepository(CastRepository, ABC):
    ...

    async def add(self, name: str, summary: str):
        stmt = select(Cast).where(Cast.name == name).limit(1)
        result = await self.session.execute(stmt)

        cast = result.scalar()

        if cast is not None:
            return cast

        cast = Cast(name=name, summary=summary)
        try:
            self.session.add(cast)
            await self.session.commit()
            await self.session.refresh(cast)
        except Exception as exc:
            log.error(exc)
        return cast
```

Y, por supuesto, se ha creado el *endpoint* que implementa la funcionalidad:

```python
@movie_router.get("/search", response_model=List[MovieOut])
async def search_movies_by_title(
        response: Response, title: str, db: AsyncSession = Depends(get_db)
):
    movie_repository = PSQLMovieRepository(db)

    movies_out = await PSQLMovieService(movie_repository).search_by_title(name=title)

    if len(movies_out) == 0:
        try:
            movies_out = await IMDBMovieAPIService(movie_repository).search_by_title(
                name=title
            )
        except MovieAPIException as e:
            log.error(e)
            raise HTTPException(status_code=404, detail=str(e))

        if len(movies_out) > 0:
            response.status_code = status.HTTP_201_CREATED

    return movies_out
```

Como se ha mencionado, este primero hace una búsqueda en la base de datos. De no existir ninguna película con el
criterio establecido, se utiliza el servicio de la API que, en caso de error, lanza una excepción. Si se encuentran
películas en la API, estas se guardan en la base de datos (junto al reparto en el microservicio *Cast*) para futuras
llamadas, y se devuelven con el código de respuesta 201.

Tras la implementación, se puede comprobar que ninguna prueba falla.

![TDD en *Cast*](img/TDD%20Tests%20Cast.png "Tests en *Cast*")

![TDD en *Movie*](img/TDD%20Tests%20Movie.png "Tests en *Movie*")

*Cabe mencionar que, como se ha mencionado anteriormente, la API implementada tiene un límite de 100 peticiones por día
en su versión gratuita, por lo que en caso de superar dicho límite la prueba fallará. Por ello, se ha ignorado en los
scripts que ejecutan las pruebas (por ejemplo en *localPipeline.ps1*).

```console
docker-compose exec movie_service python -m pytest tests -p no:warnings --cov=app --ignore=tests/integration/test_movie.py 
```

Así, el desarrollo de la funcionalidad está terminado.

Respecto a la Metodología Ágil, así queda el sprint:

![Sprint tras funcionalidad](img/sprint%20end%20functionallity.png "Sprint tras funcionalidad")

Y el tablero:

![Tablero tras funcionalidad](img/board%20end%20functionallity.png "Tablero tras funcionalidad")

## Pipeline de Integración Continua

Para automatizar la ejecución de los stages **Build** y **Test**, se ha desarrollado un **Pipeline** en GitLab. Para
ello, se ha tratado de crear diferentes variables y plantillas para los diferentes stages, para que sean reusables y no
haya código repetido.

Primero se han definido los diferentes stages del pipeline, y variables para nombrar las diferentes imágenes de los
contenedores:

```yaml
stages:
  - build
  - test

variables:
  IMAGE_LATEST: ${CI_REGISTRY_IMAGE}/${CONTAINER_NAME}:latest
  IMAGE_TEST: ${CI_REGISTRY_IMAGE}/${CONTAINER_NAME}:test_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  DOCKER_DRIVER: overlay2
```

Después, se ha creado una plantilla que extenderán otras plantillas para definir cuándo se ejecutarán los stages:

```yaml
.when:
  only:
    refs:
      - tags
    changes:
      - cast-service/**/*
      - movie-service/**/*
      - .gitlab-ci.yml
  except:
    - /^feature\//
    - /^bug_fix\//
```

Brevemente, esta parte hace que los trabajos que la extiendan solo se ejecuten en eventos que sean etiquetas, y solo si
ha habido cambios en los archivos dentro de los directorios *cast_service* y *movie_service*, o en el archivo *
.gilab-ci.yml*. La sección de *except* marca que los trabajos no se ejecutarán en ramas que empiecen con *feature/* o
*bug_fix/*.

Tras esto, se han creado plantillas para los diferentes stages.

La plantilla para **Build**:

```yaml
.build:
  stage: build
  extends: .when
  services:
    - docker:dind
  before_script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
  script:
    - docker pull ${IMAGE_LATEST} || true
    - cd ${CONTAINER_NAME}-service/
    - docker build
      --cache-from ${IMAGE_LATEST}
      --tag ${IMAGE_TEST}
      --file Dockerfile.prod
      .
    - docker push ${IMAGE_TEST}
```

Esta plantilla sirve para construir la imagen de Docker de un servicio específico y luego enviarla al registro de
contenedores de GitLab, para su posterior uso en despliegues y pruebas.

La plantilla para **Test**:

```yaml
.test:
  stage: test
  extends: .when
  services:
    - postgres:latest
  image: ${IMAGE_TEST}
  variables:
    APP_ENVIRONMENT: local_test
    POSTGRES_DB: ${CONTAINER_NAME}_db_dev
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: ""
    POSTGRES_HOST_AUTH_METHOD: trust
    DATABASE_LOCAL_TEST_URL: postgresql+asyncpg://runner@postgres:5432/${CONTAINER_NAME}_db_dev
  script:
    - cd /home/app/${CONTAINER_NAME}
    - python -m pytest tests -p no:warnings --cov=app --ignore=tests/integration/test_movie.py
    - flake8
    - black . --check
    - isort . --check-only
    - coverage report --fail-under=70
    - coverage xml -o ${CI_PROJECT_DIR}/${CONTAINER_NAME}-coverage.xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: ${CI_PROJECT_DIR}/${CONTAINER_NAME}-coverage.xml
```

Esta plantilla sirve para ejecutar pruebas y verificar la calidad del código de un servicio. Además de las pruebas,
el trabajo también incluye comprobaciones de estilo de código y verificaciones de cobertura de código, haciendo que sea
necesario un porcentaje de cobertura de mínimo el 70%. La sección **coverage** especifica una expresión regular que se
utiliza para buscar el porcentaje total de cobertura en el informe de cobertura generado en la sección anterior. La
expresión regular busca cualquier cadena que comience con la palabra "total" y termine con un porcentaje de cobertura.
Finalmente, la sección artifacts especifica que se debe subir el informe de cobertura generado en el archivo
*coverage.xml*.

Con ello, los trabajos para los microservicios se definen de la siguiente manera, habiendo reducido en gran medida
duplicidades en el archivo del pipeline:

```yaml
build cast:
  extends: .build
  variables:
    CONTAINER_NAME: cast

build movie:
  extends: .build
  variables:
    CONTAINER_NAME: movie

test cast:
  extends: .test
  variables:
    CONTAINER_NAME: cast

test movie:
  extends: .test
  variables:
    CONTAINER_NAME: movie
```

El pipeline y sus trabajos tras ser ejecutado:

![Ejecución del pipeline CI](img/pipeline%20ci.png "Ejecución del pipeline CI")

![Trabajos del pipeline CI](img/pipeline%20ci%20jobs.png "Trabajos del pipeline CI")

Respecto a la Metodología Ágil, así queda el sprint:

![Sprint tras pipeline CI](img/sprint%20end%20ci.png "Sprint tras pipeline CI")

Y el tablero:

![Tablero tras pipeline CI](img/board%20end%20ci.png "Tablero tras pipeline CI")

## Conjunto de Pruebas de Humo

Con el objetivo de comprobar si las nuevas versiones de los microservicios tienen fallos críticos en el sistema que
impiden su funcionamiento básico, también se han identificado las funcionalidades clave del sistema, e implementado un
Conjunto de Pruebas de Humo. Las funcionalidades clave del sistema son:

- Ambos *health-check* de los microservicios, ya que sirven para comprobar que el sistema completo está en marcha.
- La funcionalidad para añadir reparto en el microservicio de *Cast*.
- La funcionalidad para añadir películas en el microservicio de *Movie*.
- La funcionalidad para buscar películas en el microservicio de *Movie*, tanto todas como por identificador.

Teniendo en cuenta estas funcionalidades, se han desarrollado diferentes pruebas en ambos microservicios, con argumentos
de configuración desde la línea de comandos para poder ser ejecutados mediante un script.

Configuración para las pruebas:

```python
import pytest


def pytest_addoption(parser):
    parser.addoption(
        "--base-url",
        action="store",
        default="http://localhost:8080",
        help="Base URL for the API tests",
    )


@pytest.fixture
def base_url(request):
    return request.config.getoption("--base-url")

```

Pruebas en el microservicio *Cast*:

```python
class TestRoutes:
    def test_health_check(self, base_url):
        url = base_url + "/api/v1/cast/health-check/"
        response = httpx.get(url)
        assert response.status_code == 200
        assert response.json()["message"] == "OK"

    def test_create_cast(self, base_url):
        url = base_url + "/api/v1/cast/"
        response = httpx.post(url, json=cast_data)
        assert response.status_code == 201
        assert response.json()["name"] == cast_data["name"]

```

Pruebas en el microservicio *Movie*:

```python
class TestRoutes:
    def test_health_check(self, base_url):
        url = base_url + "/api/v1/movie/health-check/"
        response = httpx.get(url)
        assert response.status_code == 200
        assert response.json()["message"] == "OK"

    def test_get_movie_by_id(self, base_url, test_create_movie_and_yield_id):
        url = base_url + f"/api/v1/movie/{test_create_movie_and_yield_id}/"
        response = httpx.get(url)
        assert response.status_code == 200
        assert response.json()["name"] == movie_data["name"]

    def test_get_movies(self, base_url):
        url = base_url + "/api/v1/movie/"
        response = httpx.get(url)
        assert response.status_code == 200
        assert len(response.json()) > 0

    @pytest.fixture
    def test_create_movie_and_yield_id(self, base_url):
        url = base_url + "/api/v1/movie/"
        response = httpx.post(url, json=movie_data)
        assert response.status_code == 201
        assert response.json()["name"] == movie_data["name"]
        saved_movie_id = response.json()["id"]
        yield saved_movie_id
```

Para que estas pruebas no se ejecuten en el Pipeline de Integración Continua ni en el Pipeline de Pruebas Local, se ha
añadido lo siguiente en la ejecución de pruebas:

```commandline
--ignore=tests/e2e
```

Para ejecutar las pruebas se ha desarrollado un script en PowerShell:

```powershell
$movie_service_ip = "localhost"
$movie_service_port = "8080"
$cast_service_ip = "localhost"
$cast_service_port = "8080"

$movie_service_url = "http://${movie_service_ip}:${movie_service_port}"
$cast_service_url = "http://${cast_service_ip}:${cast_service_port}"

python -m pytest cast-service/tests/e2e -p no:warnings --base-url = ${cast_service_url} -vv
python -m pytest movie-service/tests/e2e -p no:warnings --base-url = ${movie_service_url
} -vv
```

**Ahora estas pruebas se ejecutan en local. Este script debe cambiarse para utilizar las URLs de los microservicios tras
ser desplegados.

A continuación se muestra la ejecución de las pruebas:

![Smoke Test Cast](img/Smoke%20Test%20Cast.png "Smoke Test Cast")

![Smoke Test Movie](img/Smoke%20Test%20Movie.png "Smoke Test Movie")

Respecto a la Metodología Ágil, así queda el sprint:

![Sprint tras Smoke Tests](img/sprint%20end%20smoke.png "Sprint tras Smoke Tests")

Y el tablero:

![Tablero tras Smoke Tests](img/board%20end%20smoke.png "Tablero tras Smoke Tests")

## Despliegue en el entorno Stage

Por último, para finalizar la práctica todo el sistema ha de ser desplegado en la nube en el entorno **Stage**, en este
caso en AWS. Para ello, primero se ha modificado el pipeline de integración continua, para añadir dos nuevos stages: uno
llamado **Delivery** para la entrega de versiones de lanzamiento de las imágenes de los contenedores en AWS ECR, y otro
llamado **Deploy** para su desplegar dichas versiones en AWS ECS. También se han creado plantillas para dichos stages
para definir trabajos de manera rápida:

```yaml
stages:
  ...
  - delivery
  - deploy
```

```yaml
.delivery:
  stage: delivery
  extends: .when
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - apk add --update --no-cache py-pip
    - pip install awscli
  script:
    - docker pull ${IMAGE_TEST}
    - docker tag ${IMAGE_TEST} ${IMAGE_RELEASE}
    - docker push ${IMAGE_RELEASE}
    - docker tag ${IMAGE_RELEASE} ${IMAGE_LATEST}
    - docker push ${IMAGE_LATEST}
    - docker tag ${IMAGE_LATEST} ${REPOSITORY_URL}:${DEPLOY_TAG}
    - $(aws ecr get-login --no-include-email --region us-east-1)
    - docker push ${REPOSITORY_URL}:${DEPLOY_TAG}
```

```yaml
.deploy:
  stage: deploy
  extends: .when
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - apk add --update --no-cache jq py-pip curl
    - pip install awscli
    - pip install pytest
  script:
    - echo `aws ecs describe-task-definition --task-definition ${CONTAINER_NAME} --region us-east-1` > input.json
    - echo $(cat input.json | jq '.taskDefinition.containerDefinitions[].image="'${REPOSITORY_URL}':'${DEPLOY_TAG}'"') > input.json
    - echo $(cat input.json | jq '.taskDefinition') > input.json
    - echo $(cat input.json | jq 'del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities) | del(.registeredAt) | del(.registeredBy)') > input.json
    - aws ecs register-task-definition --cli-input-json file://input.json --region us-east-1
    - revision=$(aws ecs describe-task-definition --task-definition ${CONTAINER_NAME} --region us-east-1 | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//' | cut -d "," -f 1)
    - aws ecs update-service --cluster ${CONTAINER_NAME} --service ${CONTAINER_NAME} --task-definition ${CONTAINER_NAME}:${revision} --region us-east-1
    - echo "Done! Revision ===> ${revision}"
```

También se han añadido diferentes variables necesarias por las nuevas plantillas:

```yaml
variables:
  ...
  IMAGE_RELEASE: ${CI_REGISTRY_IMAGE}/${CONTAINER_NAME}:release_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  DEPLOY_TAG: ${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  REPOSITORY_URL: ${ECR_URL}/${CONTAINER_NAME}
```

Y otras en la configuración de Integración y Despliegue Continuo de GitLab:

![CICD Variables](img/CICD%20variables.png "CICD Variables")

Con todo esto, utilizando las plantillas los trabajos quedan de la siguiente forma:

```yaml
delivery cast:
  extends: .delivery
  variables:
    CONTAINER_NAME: cast

delivery movie:
  extends: .delivery
  variables:
    CONTAINER_NAME: movie

deploy cast:
  extends: .deploy
  variables:
    CONTAINER_NAME: cast

deploy movie:
  extends: .deploy
  variables:
    CONTAINER_NAME: movie
```

Por supuesto, se han creado los repositorios en AWS ECR; los servicios, clústeres y definiciones de tareas necesarias (
con configuración como variables de entorno para los contenedores) en AWS ECS para la consecución de estos stages (con
los nombres de los contenedores para que funcionen); y las bases de datos para cada microservicio:

![Repositorios AWS ECR](img/AWS%20ECR%20repos.png "Repositorios AWS ECR")

![Tareas AWS ECS](img/AWS%20ECS%20tasks.png "Tareas AWS ECS")

![Clústeres AWS ECS](img/AWS%20ECS%20clusters.png "Clústeres AWS ECS")

![Servicio Cast AWS ECS](img/AWS%20ECS%20cast%20service.png "Servicio Cast AWS ECS")

![Servicio Movie AWS ECS](img/AWS%20ECS%20movie%20service.png "Servicio Movie AWS ECS")

![Bases de Datos AWS RDS](img/AWS%20RDS%20dbs.png "Bases de Datos AWS RDS")

Con esto, cuando se ejecuta el pipeline completo, los microservicios se despliegan automáticamente, como se puede ver
en las siguientes imágenes:

![*Cast* desplegado](img/Despliegue%20de%20Cast.png "*Cast* desplegado")

![*Movie* desplegado](img/Despliegue%20de%20Movie.png "*Movie* desplegado")

Y la ejecución del pipeline se ve así:

![Ejecución pipeline completo](img/full%20pipeline.png "Ejecución pipeline completo")

![Ejecución de trabajos del pipeline completo](img/full%20pipeline%20jobs.png "Ejecución de trabajos del pipeline completo")

Finalmente, respecto a la Metodología Ágil, así queda el sprint:

![Sprint tras despliegue](img/sprint%20end%20stage.png "Sprint tras despliegue")

Y el tablero:

![Tablero tras despliegue](img/board%20end%20stage.png "Tablero tras despliegue")

Con esto, la práctica ha sido completada.