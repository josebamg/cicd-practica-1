import logging
from typing import List

from fastapi import APIRouter, Depends, HTTPException, Response, status
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_db
from app.domain.exception.movie_api_exception import MovieAPIException
from app.domain.exception.movie_not_found_exception import MovieNotFoundException
from app.domain.model.movie import MovieIn, MovieOut, MovieUpdate
from app.infrastructure.repository.psql_movie_repository import PSQLMovieRepository
from app.infrastructure.service.imdb_movie_api_service import IMDBMovieAPIService
from app.infrastructure.service.psql_movie_service import PSQLMovieService
from app.infrastructure.utils.service import is_cast_present

log = logging.getLogger(__name__)

movie_router = APIRouter()


@movie_router.post("/", response_model=MovieOut, status_code=201)
async def create_movie(payload: MovieIn, db: AsyncSession = Depends(get_db)):
    for cast_id in payload.cast_ids:
        if not is_cast_present(cast_id):
            raise HTTPException(
                status_code=404, detail=f"Cast with given ID {cast_id} not found"
            )

    movie_out = await PSQLMovieService(PSQLMovieRepository(db)).add(payload)

    return movie_out


@movie_router.get("/", response_model=List[MovieOut])
async def get_movies(db: AsyncSession = Depends(get_db)):
    return await PSQLMovieService(PSQLMovieRepository(db)).get_all()


@movie_router.get("/{id}/", response_model=MovieOut)
async def get_movie(id: int, db: AsyncSession = Depends(get_db)):
    try:
        movie_out = await PSQLMovieService(PSQLMovieRepository(db)).get_by_id(id)
    except MovieNotFoundException as e:
        log.error(e)
        raise HTTPException(status_code=404, detail=str(e))
    return movie_out


@movie_router.get("/search", response_model=List[MovieOut])
async def search_movies_by_title(
    response: Response, title: str, db: AsyncSession = Depends(get_db)
):
    movie_repository = PSQLMovieRepository(db)

    movies_out = await PSQLMovieService(movie_repository).search_by_title(name=title)

    if len(movies_out) == 0:
        try:
            movies_out = await IMDBMovieAPIService(movie_repository).search_by_title(
                name=title
            )
        except MovieAPIException as e:
            log.error(e)
            raise HTTPException(status_code=404, detail=str(e))

        if len(movies_out) > 0:
            response.status_code = status.HTTP_201_CREATED

    return movies_out


@movie_router.put("/{id}/", response_model=MovieOut)
async def update_movie(
    id: int, payload: MovieUpdate, db: AsyncSession = Depends(get_db)
):
    try:
        movie_out = await PSQLMovieService(PSQLMovieRepository(db)).get_by_id(id)
    except MovieNotFoundException as e:
        log.error(e)
        raise HTTPException(status_code=404, detail=str(e))

    update_data = payload.dict(exclude_unset=True)

    if "cast_ids" in update_data:
        for cast_id in payload.cast_ids:
            if not is_cast_present(cast_id):
                raise HTTPException(
                    status_code=404, detail=f"Cast with given ID {cast_id} not found"
                )

    movie_in_db = MovieIn(**movie_out.dict())

    updated_movie = movie_in_db.copy(update=update_data)

    movie_out = await PSQLMovieService(PSQLMovieRepository(db)).update(
        id, updated_movie
    )

    return movie_out


@movie_router.delete("/{id}/", response_model=MovieOut)
async def delete_movie(id: int, db: AsyncSession = Depends(get_db)):
    try:
        movie_out = await PSQLMovieService(PSQLMovieRepository(db)).delete(id)
    except MovieNotFoundException as e:
        log.error(e)
        raise HTTPException(status_code=404, detail=str(e))

    return movie_out
