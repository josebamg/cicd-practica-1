import logging

from fastapi import FastAPI

from app import db
from app.application.api.health_router import health_router
from app.application.api.movie_router import movie_router

log = logging.getLogger("uvicorn")


def create_app() -> FastAPI:
    server = FastAPI(
        openapi_url="/api/v1/movie/openapi.json", docs_url="/api/v1/movie/docs"
    )

    server.include_router(health_router, prefix="/api/v1/movie", tags=["health check"])
    server.include_router(movie_router, prefix="/api/v1/movie", tags=["movie"])

    return server


app = create_app()


@app.on_event("startup")
async def startup_event():
    log.info("Starting up...")
    await db.init_sql_model()


@app.on_event("shutdown")
async def shutdown_event():
    log.info("Shutting down...")
