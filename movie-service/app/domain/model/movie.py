from datetime import datetime
from typing import List, Optional

from pydantic import BaseModel
from sqlalchemy import ARRAY, Column, DateTime, Integer, String

from app.db import Base


class MovieIn(BaseModel):
    name: str
    plot: str
    genres: List[str]
    cast_ids: List[int]


class MovieOut(MovieIn):
    id: int
    created_at: datetime


class MovieUpdate(MovieIn):
    name: Optional[str] = None
    plot: Optional[str] = None
    genres: Optional[List[str]] = None
    cast_ids: Optional[List[int]] = None


class Movie(Base):
    __tablename__ = "Movies"
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    plot = Column(String(250))
    genres = Column(ARRAY(String))
    cast_ids = Column(ARRAY(Integer))
    created_at = Column(DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return f'<Movie "{self.name}">'


def movie_to_out(movie: Movie):
    return MovieOut(
        id=movie.id,
        name=movie.name,
        plot=movie.plot,
        genres=movie.genres,
        cast_ids=movie.cast_ids,
        created_at=movie.created_at,
    )
