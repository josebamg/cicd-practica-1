import abc
from typing import List

from app.domain.model.movie import MovieIn, MovieOut


class MovieService(abc.ABC):
    @abc.abstractmethod
    async def get_by_id(self, id: int) -> MovieOut:
        raise NotImplementedError

    @abc.abstractmethod
    async def add(self, movie_in: MovieIn) -> MovieOut:
        raise NotImplementedError

    @abc.abstractmethod
    async def get_all(self) -> List[MovieOut]:
        raise NotImplementedError

    @abc.abstractmethod
    async def delete(self, id: int) -> MovieOut:
        raise NotImplementedError

    @abc.abstractmethod
    async def update(self, id: int, movie_update: MovieIn) -> MovieOut:
        raise NotImplementedError

    @abc.abstractmethod
    async def search_by_title(self, name: str) -> List[MovieOut]:
        raise NotImplementedError
