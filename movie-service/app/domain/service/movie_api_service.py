import abc
from typing import List

from app.domain.model.movie import MovieOut


class MovieAPIService(abc.ABC):
    @abc.abstractmethod
    async def search_by_title(self, name: str) -> List[MovieOut]:
        raise NotImplementedError
