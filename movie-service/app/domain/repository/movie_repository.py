import abc
from typing import List

from app.domain.model.movie import Movie


class MovieRepository(abc.ABC):
    @abc.abstractmethod
    async def get_by_id(self, id: int) -> Movie:
        raise NotImplementedError

    @abc.abstractmethod
    async def add(
        self, name: str, plot: str, genres: List[str], cast_ids: List[int]
    ) -> Movie:
        raise NotImplementedError

    @abc.abstractmethod
    async def get_all(self) -> List[Movie]:
        raise NotImplementedError

    @abc.abstractmethod
    async def delete(self, id: int) -> Movie:
        raise NotImplementedError

    @abc.abstractmethod
    async def update(self, id: int, movie_update: dict) -> Movie:
        raise NotImplementedError

    @abc.abstractmethod
    async def search_by_title(self, name: str) -> List[Movie]:
        raise NotImplementedError
