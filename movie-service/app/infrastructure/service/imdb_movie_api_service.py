import os
from abc import ABC
from typing import List

import httpx

from app.domain.exception.movie_not_found_exception import MovieNotFoundException
from app.domain.model.movie import Movie, MovieOut, movie_to_out
from app.domain.repository.movie_repository import MovieRepository
from app.domain.service.movie_api_service import MovieAPIService

# from app.infrastructure.utils.service import add_cast

IMDB_API_KEY = os.environ.get("IMDB_API_KEY")


class IMDBMovieAPIService(MovieAPIService, ABC):
    def __init__(self, repository: MovieRepository):
        self.repository = repository

    async def search_by_title(self, name: str) -> List[MovieOut]:
        response = httpx.get(
            f"https://imdb-api.com/en/API/SearchMovie/{IMDB_API_KEY}/{name}"
        )

        response_json = response.json()

        if response_json["errorMessage"] != "":
            raise MovieNotFoundException(
                "Movies with the given title could not be found in the DB, "
                f"and the IMDB API gave the following error: {response_json['errorMessage']}"
            )

        movies = []

        for result in response.json()["results"][0:1]:
            movie_response = httpx.get(
                f"https://imdb-api.com/en/API/Title/{IMDB_API_KEY}/{result['id']}"
            )
            movie_json = movie_response.json()
            cast_ids = []

            # if movie_json["starList"] is not None:
            #     for star in movie_json["starList"]:
            #         star_response = httpx.get(
            #             f"https://imdb-api.com/en/API/Name/{IMDB_API_KEY}/{star['id']}"
            #         )
            #
            #         star_json = star_response.json()
            #
            #         cast = add_cast(
            #             cast_in={
            #                 "name": star_json["name"],
            #                 "summary": star_json["summary"],
            #             }
            #         )
            #
            #         cast_ids.append(cast["id"])

            genres = []

            if movie_json["genres"] is not None:
                genres = movie_json["genres"].split(", ")

            movie = await self.repository.add(
                name=movie_json["fullTitle"],
                plot=movie_json["plot"],
                genres=genres,
                cast_ids=cast_ids,
            )

            if isinstance(movie, Movie):
                movies.append(movie)

        movies_out = list(map(movie_to_out, movies))

        return movies_out
