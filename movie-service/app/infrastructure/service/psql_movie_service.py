from abc import ABC

from app.domain.model.movie import MovieIn, movie_to_out
from app.domain.repository.movie_repository import MovieRepository
from app.domain.service.movie_service import MovieService


class PSQLMovieService(MovieService, ABC):
    def __init__(self, repository: MovieRepository):
        self.repository = repository

    async def get_by_id(self, id: int):
        movie = await self.repository.get_by_id(id)
        movie_out = movie_to_out(movie)

        return movie_out

    async def add(self, movie_in: MovieIn):
        movie = await self.repository.add(
            name=movie_in.name,
            plot=movie_in.plot,
            genres=movie_in.genres,
            cast_ids=movie_in.cast_ids,
        )
        movie_out = movie_to_out(movie)

        return movie_out

    async def get_all(self):
        movies = await self.repository.get_all()
        movies_out = list(map(movie_to_out, movies))

        return movies_out

    async def delete(self, id: int):
        movie = await self.repository.delete(id)
        movie_out = movie_to_out(movie)

        return movie_out

    async def update(self, id: int, movie_update: MovieIn):
        movie = await self.repository.update(id=id, movie_update=movie_update.dict())
        movie_out = movie_to_out(movie)

        return movie_out

    async def search_by_title(self, name: str):
        movies = await self.repository.search_by_title(name=name)
        movies_out = list(map(movie_to_out, movies))
        return movies_out
