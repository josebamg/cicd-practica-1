import logging
from abc import ABC
from typing import List

from sqlalchemy import select, update
from sqlalchemy.ext.asyncio import AsyncSession

from app.domain.exception.movie_not_found_exception import MovieNotFoundException
from app.domain.model.movie import Movie
from app.domain.repository.movie_repository import MovieRepository

log = logging.getLogger(__name__)


class PSQLMovieRepository(MovieRepository, ABC):
    def __init__(self, session: AsyncSession):
        self.session = session

    async def get_by_id(self, id: int):
        movie = await self.session.get(Movie, id)
        if movie is None:
            raise MovieNotFoundException(f"Movie with ID {id} not found")
        return movie

    async def add(self, name: str, plot: str, genres: List[str], cast_ids: List[int]):
        movie = Movie(name=name, plot=plot, genres=genres, cast_ids=cast_ids)
        try:
            self.session.add(movie)
            await self.session.commit()
            await self.session.refresh(movie)
        except Exception as exc:
            log.error(exc)
        return movie

    async def get_all(self):
        stmt = select(Movie)
        movies = await self.session.execute(stmt)
        return movies.scalars().all()

    async def delete(self, id: int):
        movie = await self.get_by_id(id)
        await self.session.delete(movie)
        await self.session.commit()
        return movie

    async def update(self, id: int, movie_update: dict):
        stmt = (
            update(Movie)
            .where(Movie.id == id)
            .values(**movie_update)
            .execution_options(synchronize_session="fetch")
        )
        await self.session.execute(stmt)
        await self.session.commit()
        movie = await self.get_by_id(id)
        return movie

    async def search_by_title(self, name: str):
        stmt = select(Movie).where(Movie.name.contains(name))
        movies = await self.session.execute(stmt)
        return movies.scalars().all()
