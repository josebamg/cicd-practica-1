import os

import httpx

CAST_SERVICE_HOST_URL = (
    os.environ.get("CAST_SERVICE_HOST_URL") or "http://cast_service:8000/api/v1/cast/"
)


def is_cast_present(cast_id: int):
    r = httpx.get(f"{CAST_SERVICE_HOST_URL}{cast_id}/")
    return True if r.status_code == 200 else False


def add_cast(cast_in: dict):
    r = httpx.post(f"{CAST_SERVICE_HOST_URL}", json=cast_in)
    return r.json()
