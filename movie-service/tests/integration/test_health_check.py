import pytest


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_health_check(test_client, anyio_backend):
    response = test_client.get("/api/v1/movie/health-check/")
    assert response.status_code == 200
    assert response.json() == {
        "message": "OK",
        "environment": "local",
        "debug": True,
        "test": True,
        # "database_url": "postgresql+asyncpg://movie_db_username:movie_db_password@movie_db/movie_db_dev",
    }
