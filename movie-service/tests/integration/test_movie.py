import pytest


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_get_movies_by_title(test_client, monkeypatch, anyio_backend):
    name = "Inception (2010)"

    response = test_client.get(f"/api/v1/movie/search?title={name}")

    assert response.status_code == 201
    assert len(response.json()) > 0

    response = test_client.get(f"/api/v1/movie/search?title={name}")

    assert response.status_code == 200
    assert response.json()[0]["name"] == name
