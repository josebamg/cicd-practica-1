import httpx
import pytest

movie_data = {
    "name": "Million Dollar Baby",
    "genres": ["Sports", "Drama", "Romance"],
    "plot": "Maggie decides to pursue her dream of becoming a boxer once she realizes she's in a real dead-end "
    "situation. Frankie is skeptical about women fighters, tells Maggie she's too old to start training, and "
    "tells her he won't train her.",
    "cast_ids": [],
}


class TestRoutes:
    def test_health_check(self, base_url):
        url = base_url + "/api/v1/movie/health-check/"
        response = httpx.get(url)
        assert response.status_code == 200
        assert response.json()["message"] == "OK"

    def test_get_movie_by_id(self, base_url, test_create_movie_and_yield_id):
        url = base_url + f"/api/v1/movie/{test_create_movie_and_yield_id}/"
        response = httpx.get(url)
        assert response.status_code == 200
        assert response.json()["name"] == movie_data["name"]

    def test_get_movies(self, base_url):
        url = base_url + "/api/v1/movie/"
        response = httpx.get(url)
        assert response.status_code == 200
        assert len(response.json()) > 0

    @pytest.fixture
    def test_create_movie_and_yield_id(self, base_url):
        url = base_url + "/api/v1/movie/"
        response = httpx.post(url, json=movie_data)
        assert response.status_code == 201
        assert response.json()["name"] == movie_data["name"]
        saved_movie_id = response.json()["id"]
        yield saved_movie_id
