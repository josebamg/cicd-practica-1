from datetime import datetime

import pytest

from app.domain.model.movie import MovieOut

movie_id = 1
name = "Million Dollar Baby"
genres = ["Sports", "Drama", "Romance"]
plot = (
    "Maggie decides to pursue her dream of becoming a boxer once she realizes she's in a real dead-end situation."
    " Frankie is skeptical about women fighters, tells Maggie she's too old to start training, and tells her he"
    " won't train her."
)
cast_ids = []
created_at = datetime.utcnow().isoformat()
test_data = {
    "id": movie_id,
    "name": name,
    "genres": genres,
    "plot": plot,
    "cast_ids": cast_ids,
    "created_at": created_at,
}


class MockPSQLMovieService:
    def __init__(self, *args, **kwargs):
        self.movie_out = MovieOut(
            id=1,
            name=name,
            plot=plot,
            genres=genres,
            cast_ids=cast_ids,
            created_at=created_at,
        )

    async def get_by_id(self, *args, **kwargs):
        return self.movie_out

    async def add(self, *args, **kwargs):
        return self.movie_out

    async def get_all(self, *args, **kwargs):
        return [self.movie_out]

    async def delete(self, *args, **kwargs):
        return self.movie_out

    async def update(self, *args, **kwargs):
        return self.movie_out


class MockIMDBMovieAPIService:
    def __init__(self, *args, **kwargs):
        self.movie_out = MovieOut(
            id=1,
            name=name,
            plot=plot,
            genres=genres,
            cast_ids=cast_ids,
            created_at=created_at,
        )

    async def get_by_title(self, *args, **kwargs):
        return self.movie_out


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_get_movies(test_client, monkeypatch, anyio_backend):
    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.get("/api/v1/movie/")
    assert response.status_code == 200
    assert response.json() == [test_data]


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_get_movie(test_client, monkeypatch, anyio_backend):
    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.get(f"/api/v1/movie/{movie_id}/")
    assert response.status_code == 200
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_add_movie(test_client, monkeypatch, anyio_backend):
    movie_in = {"name": name, "plot": plot, "genres": genres, "cast_ids": cast_ids}

    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.post("/api/v1/movie/", json=movie_in)
    assert response.status_code == 201
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_update_movie(test_client, monkeypatch, anyio_backend):
    movie_update = {"name": name, "plot": plot, "genres": genres, "cast_ids": cast_ids}

    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.put(f"/api/v1/movie/{movie_id}/", json=movie_update)
    assert response.status_code == 200
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_delete_movie(test_client, monkeypatch, anyio_backend):
    monkeypatch.setattr(
        "app.application.api.movie_router.PSQLMovieService", MockPSQLMovieService
    )

    response = test_client.delete(f"/api/v1/movie/{movie_id}/")
    assert response.status_code == 200
    assert response.json() == test_data
