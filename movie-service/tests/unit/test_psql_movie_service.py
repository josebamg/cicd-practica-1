import pytest

from app.domain.exception.movie_not_found_exception import MovieNotFoundException
from app.domain.model.movie import MovieIn
from app.infrastructure.repository.psql_movie_repository import PSQLMovieRepository
from app.infrastructure.service.psql_movie_service import PSQLMovieService

name = "Million Dollar Baby"
genres = ["Sports", "Drama", "Romance"]
plot = (
    "Maggie decides to pursue her dream of becoming a boxer once she realizes she's in a real dead-end situation."
    " Frankie is skeptical about women fighters, tells Maggie she's too old to start training, and tells her he"
    " won't train her."
)
cast_ids = []


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_create_and_get_movie(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    movie_in = MovieIn(name=name, genres=genres, plot=plot, cast_ids=cast_ids)

    movie_out = await movie_service.add(movie_in=movie_in)

    assert movie_out is not None
    assert movie_out.name == name

    movie_out = await movie_service.get_by_id(id=1)

    assert movie_out is not None
    assert movie_out.name == name
    assert movie_out.id == 1


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_get_movies(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    movies_out = await movie_service.get_all()

    assert movies_out is not None
    assert len(movies_out) == 0

    movie_in = MovieIn(name=name, genres=genres, plot=plot, cast_ids=cast_ids)
    movie_out = await movie_service.add(movie_in=movie_in)

    movies_out = await movie_service.get_all()

    assert movies_out is not None
    assert movies_out[0].name == movie_out.name


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_update_movie(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    movie_in = MovieIn(name=name, genres=genres, plot=plot, cast_ids=cast_ids)
    movie_update = MovieIn(
        name="Kill Bill", genres=genres, plot=plot, cast_ids=cast_ids
    )

    movie_out = await movie_service.add(movie_in=movie_in)
    movie_updated = await movie_service.update(
        id=movie_out.id, movie_update=movie_update
    )

    assert movie_updated is not None
    assert movie_updated.id == movie_out.id
    assert movie_updated.name != movie_out.name


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_delete_movie(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    movie_in = MovieIn(name=name, genres=genres, plot=plot, cast_ids=cast_ids)

    movie_out = await movie_service.add(movie_in=movie_in)
    movies_out = await movie_service.get_all()

    assert movies_out is not None
    assert len(movies_out) == 1

    movie_deleted = await movie_service.delete(id=movie_out.id)
    movies_out = await movie_service.get_all()

    assert movie_deleted is not None
    assert movie_deleted.id == 1
    assert len(movies_out) == 0


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_movie_not_found_exception_thrown(test_db, anyio_backend):
    movie_service = PSQLMovieService(PSQLMovieRepository(test_db))

    with pytest.raises(MovieNotFoundException):
        await movie_service.get_by_id(id=1)
