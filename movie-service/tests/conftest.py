import pytest
from starlette.testclient import TestClient

from app.db import Base, async_session_generator
from app.db import engine as async_engine
from app.main import create_app


async def drop_db():
    async with async_engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)


async def reset_db():
    async with async_engine.begin() as conn:
        await conn.run_sync(Base.metadata.drop_all)
        await conn.run_sync(Base.metadata.create_all)


@pytest.fixture
def test_client():
    app = create_app()
    with TestClient(app) as test_client:
        yield test_client


@pytest.fixture
async def test_db():
    await reset_db()
    session_local = async_session_generator()
    db = session_local()

    try:
        yield db
        await db.commit()
    except Exception:
        await db.rollback()
    finally:
        await db.close()
