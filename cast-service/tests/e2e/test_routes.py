import httpx

cast_data = {"name": "Morgan Freeman", "summary": "Born in EEUU"}


class TestRoutes:
    def test_health_check(self, base_url):
        url = base_url + "/api/v1/cast/health-check/"
        response = httpx.get(url)
        assert response.status_code == 200
        assert response.json()["message"] == "OK"

    def test_create_cast(self, base_url):
        url = base_url + "/api/v1/cast/"
        response = httpx.post(url, json=cast_data)
        assert response.status_code == 201
        assert response.json()["name"] == cast_data["name"]
