import pytest

from app.domain.exception.cast_not_found_exception import CastNotFoundException
from app.domain.model.cast import CastIn
from app.infrastructure.repository.psql_cast_repository import PSQLCastRepository
from app.infrastructure.service.psql_cast_service import PSQLCastService

name = "Morgan Freeman"
summary = "Born in EEUU"


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_create_and_get_cast(test_db, anyio_backend):
    cast_service = PSQLCastService(PSQLCastRepository(test_db))

    cast_in = CastIn(name=name, summary=summary)

    cast_out = await cast_service.add(cast_in=cast_in)

    assert cast_out is not None
    assert cast_out.name == name

    cast_out = await cast_service.get_by_id(id=1)
    assert cast_out is not None
    assert cast_out.name == name
    assert cast_out.id == 1


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_service_cast_not_found_exception_thrown(test_db, anyio_backend):
    cast_service = PSQLCastService(PSQLCastRepository(test_db))

    with pytest.raises(CastNotFoundException):
        await cast_service.get_by_id(id=1)
