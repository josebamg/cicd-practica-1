from datetime import datetime

import pytest

from app.domain.model.cast import CastOut

name = "Morgan Freeman"
summary = "Born in EEUU"
cast_id = 1
created_at = datetime.utcnow().isoformat()
test_data = {
    "id": cast_id,
    "name": name,
    "summary": summary,
    "created_at": created_at,
}


class MockPSQLCastService:
    def __init__(self, *args, **kwargs):
        self.cast_out = CastOut(
            id=cast_id, name=name, summary=summary, created_at=created_at
        )

    async def get_by_id(self, *args, **kwargs):
        return self.cast_out

    async def add(self, *args, **kwargs):
        return self.cast_out


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_create_cast(test_client, monkeypatch, anyio_backend):
    monkeypatch.setattr(
        "app.application.api.cast_router.PSQLCastService", MockPSQLCastService
    )

    response = test_client.get(f"/api/v1/cast/{cast_id}/")
    assert response.status_code == 200
    assert response.json() == test_data


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
async def test_get_cast(test_client, monkeypatch, anyio_backend):
    cast_in = {"name": name, "summary": summary}

    monkeypatch.setattr(
        "app.application.api.cast_router.PSQLCastService", MockPSQLCastService
    )

    response = test_client.post("/api/v1/cast/", json=cast_in)
    assert response.status_code == 201
    assert response.json() == test_data
