import pytest


@pytest.mark.parametrize("anyio_backend", ["asyncio"])
def test_health_check(test_client, anyio_backend):
    response = test_client.get("/api/v1/cast/health-check/")
    assert response.status_code == 200
    assert response.json() == {
        "message": "OK",
        "environment": "local",
        "debug": True,
        "test": True,
        # "database_url": "postgresql+asyncpg://cast_db_username:cast_db_password@cast_db/cast_db_dev",
    }
