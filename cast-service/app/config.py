import os


class BaseConfig:
    DEBUG = False
    TESTING = False
    APP_ENVIRONMENT = "production"
    SQLALCHEMY_DATABASE_URL = None


class ProductionConfig(BaseConfig):
    APP_ENVIRONMENT = "production"
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_PROD_URL")


class DevelopmentConfig(ProductionConfig):
    DEBUG = True
    APP_ENVIRONMENT = "development"
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_DEV_URL")


class TestConfig(BaseConfig):
    TESTING = True
    DEBUG = False
    APP_ENVIRONMENT = "local"
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_TEST_URL")


class LocalTestConfig(BaseConfig):
    DEBUG = True
    APP_ENVIRONMENT = "local"
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_LOCAL_TEST_URL")
    TESTING = True


class LocalConfig(BaseConfig):
    APP_ENVIRONMENT = "local"
    SQLALCHEMY_DATABASE_URL = os.environ.get("DATABASE_LOCAL_URL")
    DEBUG = True


CONFIGS = {
    "production": ProductionConfig,
    "development": DevelopmentConfig,
    "test": TestConfig,
    "local_test": LocalTestConfig,
    "local": LocalConfig,
}


def load_config() -> BaseConfig:
    return CONFIGS.get(os.getenv("APP_ENVIRONMENT"), LocalConfig)()


app_config = load_config()
