import logging
from contextlib import asynccontextmanager

from sqlalchemy import NullPool
from sqlalchemy.ext.asyncio import async_sessionmaker, create_async_engine
from sqlalchemy.orm import declarative_base

from app.config import app_config

log = logging.getLogger(__name__)

Base = declarative_base()

engine = create_async_engine(
    app_config.SQLALCHEMY_DATABASE_URL, echo=True, future=True, poolclass=NullPool
)


def async_session_generator():
    return async_sessionmaker(engine)


@asynccontextmanager
async def get_session():
    try:
        async_session = async_session_generator()
        async with async_session() as session:
            yield session
    except Exception:
        await session.rollback()
        raise
    finally:
        await session.close()


async def get_db():
    db = async_session_generator()
    async with db() as session:
        try:
            yield session
            await session.commit()
        except Exception as e:
            log.debug(e)
            await session.rollback()
        finally:
            await session.close()


async def init_sql_model():
    db = async_session_generator()

    async with db() as session:
        async with engine.begin() as conn:
            await conn.run_sync(Base.metadata.create_all)
        try:
            await session.commit()
        except Exception as e:
            log.debug(e)
            await session.rollback()
        finally:
            await session.close()
