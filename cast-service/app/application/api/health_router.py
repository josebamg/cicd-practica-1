from fastapi import APIRouter, Depends

from app.config import BaseConfig, load_config

health_router = APIRouter()


@health_router.get("/health-check/")
def healthcheck(config: BaseConfig = Depends(load_config)):
    return {
        "message": "OK",
        "environment": config.APP_ENVIRONMENT,
        "debug": config.DEBUG,
        "test": config.TESTING,
        # "database_url": config.SQLALCHEMY_DATABASE_URL,
    }
