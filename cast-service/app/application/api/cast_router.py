import logging

from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.db import get_db
from app.domain.exception.cast_not_found_exception import CastNotFoundException
from app.domain.model.cast import CastIn, CastOut
from app.infrastructure.repository.psql_cast_repository import PSQLCastRepository
from app.infrastructure.service.psql_cast_service import PSQLCastService

log = logging.getLogger(__name__)

cast_router = APIRouter()


@cast_router.post("/", response_model=CastOut, status_code=201)
async def create_cast(payload: CastIn, db: AsyncSession = Depends(get_db)):
    cast_out = await PSQLCastService(PSQLCastRepository(db)).add(payload)

    return cast_out


@cast_router.get("/{id}/", response_model=CastOut)
async def get_cast(id: int, db: AsyncSession = Depends(get_db)):
    try:
        cast_out = await PSQLCastService(PSQLCastRepository(db)).get_by_id(id)
    except CastNotFoundException as e:
        log.error(e)
        raise HTTPException(status_code=404, detail=str(e))
    return cast_out
