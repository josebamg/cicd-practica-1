import logging
from abc import ABC

from app.domain.model.cast import CastIn, cast_to_out
from app.domain.repository.cast_repository import CastRepository
from app.domain.service.cast_service import CastService

log = logging.getLogger(__name__)


class PSQLCastService(CastService, ABC):
    def __init__(self, repository: CastRepository):
        self.repository = repository

    async def get_by_id(self, id: id):
        cast = await self.repository.get_by_id(id=id)
        cast_out = cast_to_out(cast)

        return cast_out

    async def add(self, cast_in: CastIn):
        cast = await self.repository.add(name=cast_in.name, summary=cast_in.summary)
        cast_out = cast_to_out(cast)

        return cast_out
