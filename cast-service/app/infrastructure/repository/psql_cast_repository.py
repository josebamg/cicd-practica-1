import logging
from abc import ABC

from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from app.domain.exception.cast_not_found_exception import CastNotFoundException
from app.domain.model.cast import Cast
from app.domain.repository.cast_repository import CastRepository

log = logging.getLogger(__name__)


class PSQLCastRepository(CastRepository, ABC):
    def __init__(self, session: AsyncSession):
        self.session = session

    async def get_by_id(self, id: int):
        cast = await self.session.get(Cast, id)
        if cast is None:
            raise CastNotFoundException(f"Cast with ID {id} not found")
        return cast

    async def add(self, name: str, summary: str):
        stmt = select(Cast).where(Cast.name == name).limit(1)
        result = await self.session.execute(stmt)

        cast = result.scalar()

        if cast is not None:
            return cast

        cast = Cast(name=name, summary=summary)
        try:
            self.session.add(cast)
            await self.session.commit()
            await self.session.refresh(cast)
        except Exception as exc:
            log.error(exc)
        return cast
