import abc

from app.domain.model.cast import Cast


class CastRepository(abc.ABC):
    @abc.abstractmethod
    async def get_by_id(self, id: int) -> Cast:
        raise NotImplementedError

    @abc.abstractmethod
    async def add(self, name: str, summary: str) -> Cast:
        raise NotImplementedError
