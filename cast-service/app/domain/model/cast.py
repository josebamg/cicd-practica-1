from datetime import datetime
from typing import Optional

from pydantic import BaseModel
from sqlalchemy import Column, DateTime, Integer, String, Text

from app.db import Base


class CastIn(BaseModel):
    name: str
    summary: Optional[str] = None


class CastOut(CastIn):
    id: int
    created_at: datetime


class CastUpdate(CastIn):
    name: Optional[str] = None


class Cast(Base):
    __tablename__ = "Casts"
    id = Column(Integer, primary_key=True)
    name = Column(String(50))
    summary = Column(Text)
    created_at = Column(DateTime, index=True, default=datetime.utcnow)

    def __repr__(self):
        return f'<Cast "{self.name}">'


def cast_to_out(cast: Cast):
    return CastOut(
        id=cast.id,
        name=cast.name,
        summary=cast.summary,
        created_at=cast.created_at,
    )
