import abc

from app.domain.model.cast import CastIn, CastOut


class CastService(abc.ABC):
    @abc.abstractmethod
    async def get_by_id(self, id: int) -> CastOut:
        raise NotImplementedError

    @abc.abstractmethod
    async def add(self, cast_in: CastIn) -> CastOut:
        raise NotImplementedError
