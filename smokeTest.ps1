#$target_group_arn = $( aws ecs describe-services --cluster froga --services fastapi-tdd-service --region us-east-1|
#        ConvertFrom-Json ).services.loadBalancers.targetGroupArn
#$elb_arn = $( aws elbv2  describe-target-groups --target-group-arn ${target_group_arn} --region us-east-1 |
#        ConvertFrom-Json ).targetGroups.loadBalancerArns
#$elb_ip = $( aws elbv2 describe-load-balancers --load-balancer-arns ${elb_arn} --region us-east-1 |
#        ConvertFrom-Json ).loadBalancers.DNSName
#echo $elb_ip
#echo "Waiting for ECS task update..."
#aws ecs wait services-stable --cluster froga --services fastapi-tdd-service --region us-east-1

$movie_service_ip = "localhost"
$movie_service_port = "8080"
$cast_service_ip = "localhost"
$cast_service_port = "8080"

$movie_service_url = "http://${movie_service_ip}:${movie_service_port}"
$cast_service_url = "http://${cast_service_ip}:${cast_service_port}"

python -m pytest cast-service/tests/e2e -p no:warnings --base-url=${cast_service_url} -vv
python -m pytest movie-service/tests/e2e -p no:warnings --base-url=${movie_service_url} -vv