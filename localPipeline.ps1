docker-compose exec cast_service python -m pytest tests -p no:warnings --cov=app --ignore=tests/e2e
docker-compose exec cast_service flake8
docker-compose exec cast_service black . --check
docker-compose exec cast_service isort . --check-only
docker-compose exec cast_service coverage report --fail-under=70

docker-compose exec movie_service python -m pytest tests -p no:warnings --cov=app  --ignore=tests/e2e
docker-compose exec movie_service flake8
docker-compose exec movie_service black . --check
docker-compose exec movie_service isort . --check-only
docker-compose exec movie_service coverage report --fail-under=70