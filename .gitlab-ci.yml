image: docker:stable

stages:
  - build
  - test
  - delivery
  - deploy

variables:
  IMAGE_LATEST: ${CI_REGISTRY_IMAGE}/${CONTAINER_NAME}:latest
  IMAGE_TEST: ${CI_REGISTRY_IMAGE}/${CONTAINER_NAME}:test_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  DOCKER_DRIVER: overlay2
  IMAGE_RELEASE: ${CI_REGISTRY_IMAGE}/${CONTAINER_NAME}:release_${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  DEPLOY_TAG: ${CI_COMMIT_SHORT_SHA}${CI_COMMIT_TAG}
  REPOSITORY_URL: ${ECR_URL}/${CONTAINER_NAME}

.when:
  only:
    refs:
      - tags
    changes:
      - cast-service/**/*
      - movie-service/**/*
      - .gitlab-ci.yml
  except:
    - /^feature\//
    - /^bug_fix\//

.build:
  stage: build
  extends: .when
  services:
    - docker:dind
  before_script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
  script:
    - docker pull ${IMAGE_LATEST} || true
    - cd ${CONTAINER_NAME}-service/
    - docker build
      --cache-from ${IMAGE_LATEST}
      --tag ${IMAGE_TEST}
      --file Dockerfile.prod
      .
    - docker push ${IMAGE_TEST}

.test:
  stage: test
  extends: .when
  services:
    - postgres:latest
  image: ${IMAGE_TEST}
  variables:
    APP_ENVIRONMENT: local_test
    POSTGRES_DB: ${CONTAINER_NAME}_db_dev
    POSTGRES_USER: runner
    POSTGRES_PASSWORD: ""
    POSTGRES_HOST_AUTH_METHOD: trust
    DATABASE_LOCAL_TEST_URL: postgresql+asyncpg://runner@postgres:5432/${CONTAINER_NAME}_db_dev
  script:
    - cd /home/app/${CONTAINER_NAME}
    - python -m pytest tests -p no:warnings --cov=app --ignore=tests/integration/test_movie.py --ignore=tests/e2e
    - flake8
    - black . --check
    - isort . --check-only
    - coverage report --fail-under=70
    - coverage xml -o ${CI_PROJECT_DIR}/${CONTAINER_NAME}-coverage.xml
  coverage: '/(?i)total.*? (100(?:\.0+)?\%|[1-9]?\d(?:\.\d+)?\%)$/'
  artifacts:
    reports:
      coverage_report:
        coverage_format: cobertura
        path: ${CI_PROJECT_DIR}/${CONTAINER_NAME}-coverage.xml

.delivery:
  stage: delivery
  extends: .when
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - docker login -u ${CI_REGISTRY_USER} -p ${CI_JOB_TOKEN} ${CI_REGISTRY}
    - apk add --update --no-cache py-pip
    - pip install awscli
  script:
    - docker pull ${IMAGE_TEST}
    - docker tag ${IMAGE_TEST} ${IMAGE_RELEASE}
    - docker push ${IMAGE_RELEASE}
    - docker tag ${IMAGE_RELEASE} ${IMAGE_LATEST}
    - docker push ${IMAGE_LATEST}
    - docker tag ${IMAGE_LATEST} ${REPOSITORY_URL}:${DEPLOY_TAG}
    - $(aws ecr get-login --no-include-email --region us-east-1)
    - docker push ${REPOSITORY_URL}:${DEPLOY_TAG}

.deploy:
  stage: deploy
  extends: .when
  image: docker:stable
  services:
    - docker:dind
  before_script:
    - apk add --update --no-cache jq py-pip curl
    - pip install awscli
    - pip install pytest
  script:
    - echo `aws ecs describe-task-definition --task-definition ${CONTAINER_NAME} --region us-east-1` > input.json
    - echo $(cat input.json | jq '.taskDefinition.containerDefinitions[].image="'${REPOSITORY_URL}':'${DEPLOY_TAG}'"') > input.json
    - echo $(cat input.json | jq '.taskDefinition') > input.json
    - echo $(cat input.json | jq 'del(.taskDefinitionArn) | del(.revision) | del(.status) | del(.requiresAttributes) | del(.compatibilities) | del(.registeredAt) | del(.registeredBy)') > input.json
    - aws ecs register-task-definition --cli-input-json file://input.json --region us-east-1
    - revision=$(aws ecs describe-task-definition --task-definition ${CONTAINER_NAME} --region us-east-1 | egrep "revision" | tr "/" " " | awk '{print $2}' | sed 's/"$//' | cut -d "," -f 1)
    - aws ecs update-service --cluster ${CONTAINER_NAME} --service ${CONTAINER_NAME} --task-definition ${CONTAINER_NAME}:${revision} --region us-east-1
    - echo "Done! Revision ===> ${revision}"

build cast:
  extends: .build
  variables:
    CONTAINER_NAME: cast

build movie:
  extends: .build
  variables:
    CONTAINER_NAME: movie

test cast:
  extends: .test
  variables:
    CONTAINER_NAME: cast

test movie:
  extends: .test
  variables:
    CONTAINER_NAME: movie

delivery cast:
  extends: .delivery
  variables:
    CONTAINER_NAME: cast

delivery movie:
  extends: .delivery
  variables:
    CONTAINER_NAME: movie

deploy cast:
  extends: .deploy
  variables:
    CONTAINER_NAME: cast

deploy movie:
  extends: .deploy
  variables:
    CONTAINER_NAME: movie